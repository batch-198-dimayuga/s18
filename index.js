// console.log("Hello World");

function addTwoNumbers(num1,num2){

    console.log(num1 + num2);
};

function subtractTwoNumbers(num1,num2){

    console.log(num1 - num2);
};

console.log("Displayed sum of 5 and 15");
addTwoNumbers(5,15);
console.log("Displayed difference of 20 and 5");
subtractTwoNumbers(20,5);

function multiplyTwoNumbers(num1,num2){
    return num1 * num2;
};

function divideTwoNumbers(num1,num2){
    return num1 / num2;
};

let product = multiplyTwoNumbers(50,10);
let quotient = divideTwoNumbers(50,10);

console.log("The product of 50 and 10: ");
console.log(product);
console.log("The quotient of 50 and 10: ");
console.log(quotient);

function areaOfCircle(num){

    return Math.PI * Math.pow(num,2);
};

let circleArea = areaOfCircle(15);
circleArea = circleArea.toFixed(2);

console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

function averageOfFourNumbers(num1,num2,num3,num4){
    return (num1 + num2 + num3 + num4)/4;
}

let averageVar = averageOfFourNumbers(20,40,60,80);

console.log("The average of 20, 40, 60, and 80:");
console.log(averageVar);

function checkIfPass(yourScore,passingScore){
    let percentage = yourScore / passingScore;
    let isPassed = percentage >= 0.75;
    return isPassed;
}

let isPassingScore = checkIfPass(38,50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);